---
layout: handbook-page-toc
title: "How to do UI Code Contributions"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Designer's Guide to Contributing UI Code Changes

Requires HTML, CSS, Terminal (CLI), and Git Knowledge. Basic Ruby and JavaScript knowledge is also encouraged.

## Start using Git on the command line

See [Start using Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) in our GitLab docs.

### Start by Installing Your GDK

**GDK provides a local GitLab instance**

The [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit) (GDK) allows you to test changes locally, on your workstation.

1. [Learn how to prepare your workstation to run GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/prepare.md)
1. [Learn how to run GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/set-up-gdk.md)
1. [GDK Commands cheatsheet](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/HELP)

### Choose a Code Editor and Start Making Small Changes

**Learn GitLab's UI templates**

Three kinds of files handle UI rendering. At its core, GitLab is a Ruby on Rails application. The Ruby on Rails application renders GitLab's front end with `.haml` files. HAML (HTML Abstraction Markup Language) is a Ruby-based HTML template system. It's easy to learn and it even closes HTML tags for you!

For Stylesheets, GitLab uses a CSS pre-processor called SASS. SASS (Syntactically Awesome Style Sheets) uses `.scss` files that handle all of the usual stuff CSS does, but with a bit more sophistication that helps us keep GitLab's CSS better organized.

Finally, for interactivity and client-side application logic, GitLab uses a framework called Vue.js. It's rare to have to change the `.vue` files unless you're changing a [Pajamas](https://design.gitlab.com/) component, or creating a new one.

### Find Small Issues and Open Merge Requests for Your Changes

**Most fixes are a CSS change away**

If you can fix it in the browser inspector, you can probably fix it for real in the GitLab codebase. Find small UI issues and submit your changes via [merge requests (MRs)](https://docs.gitlab.com/ee/user/project/merge_requests/). Don't worry, you won't break anything, and a reviewer will always help you check your code before it ships.

### Terminal (CLI) Cheatsheet

* [Command Line basic commands](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) in our GitLab docs.
* [Basic Git commands](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#basic-git-commands) in our GitLab docs.

### Video walkthrough

Check out this video walkthrough on [Contributing to GitLab (Designer Edition!)](https://www.youtube.com/watch?v=SSo97VwVn4Y&feature=youtu.be) by [Annabel Dunstone Gray](https://gitlab.com/annabeldunstone).

---

This page is adapted from a [beautifully designed PDF](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/misc/infographics/How_to_Contribute_UI_Code_to_GitLab.pdf) created by [`@jj-ramirez`](https://gitlab.com/jj-ramirez) 😃
