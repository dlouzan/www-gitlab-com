---
layout: handbook-page-toc
title: Working With GitLab Support
---

## Overview

The purpose of this page is to direct GitLab team members outside of Support on what GitLab Support does, how to get in contact with us, and where to direct common requests that require our involvement. **Are you a customer looking for technical support? If so, please visit the [Support Page](/support/) instead.**

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Support's Purpose

GitLab Support provides technical support GitLab.com and Self-Managed GitLab customers. We do **not** provide support for GitLab team members who are experiencing IT (1Password, Slack, Mac, etc.) issues. If you require assistance with issues of that nature, please contact [IT Help](https://about.gitlab.com/handbook/business-ops/it-help/).

## Contacting GitLab Support Internally

For general questions regarding GitLab ("Can GitLab do x?", "How do I do y with GitLab?") please ask in the [#questions](https://gitlab.slack.com/messages/questions) Slack channel. Doing so ensures that [everyone can contribute](https://about.gitlab.com/company/strategy/#mission) to an answer. If you're not getting one and believe that GitLab Support can assist, try cross-posting your question in the [relevant GitLab Support channel](#on-slack). However, keep in mind that those channels are specifically for questions *about* the various GitLab Support teams, not for questions about GitLab, the product. If you're working with a customer that requires technical support, please advise them to [contact GitLab Support](#requesting-support-for-customers).

### Support Team Meta Project

If you'd like to ask a longer term or larger scope question, propose an idea to GitLab Support, discuss something with us, or suggest an improvement or change to any of our workflows, please visit the [issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues) of the **[Support Team Meta](https://gitlab.com/gitlab-com/support/support-team-meta)** project and create an issue there. Please keep in mind that it is open to the community and as such **should not contain any sensitive information**, so links to Zendesk or other references are encouraged.

### On Slack

The following channels are where GitLab Support can be found on Slack and are the best places to reach us, depending on what you need.

- [#support_gitlab-com](https://gitlab.slack.com/messages/C4XFU81LG/) - This channel is specifically for the GitLab.com support team. They handle GitLab.com account and subscription support along with GitHost. It should be used if you have a quick question about the GitLab.com Support Team, specifically.
- [#support_self-managed](https://gitlab.slack.com/messages/support_self-managed/) - This channel is specifically for the self-managed support team. They handle self-managed production issues, triage bugs, and self-managed emergencies, among other things.
- [#githost](https://gitlab.slack.com/messages/githost/) - This channel handles monitoring for GitHost instances.
- [#support-managers](https://gitlab.slack.com/messages/CBVAE1L48/) - This channel is specifically for support managers.
- [#zd-gitlab-com-feed](https://gitlab.slack.com/messages/CADGU8CG1/) - Feed of all GitLab.com Zendesk ticket activities.
- [#zd-self-managed-feed](https://gitlab.slack.com/messages/C1CKSUTL5/) - Feed of all self-managed Zendesk ticket activities.

In order to attract GitLab Support's attention on Slack, you can use the team handles, mentioning multiple team members in a message or a thread where our [**urgent** attention](https://about.gitlab.com/handbook/communication/#be-respectful-of-others-time) is needed. Support team handles are:

- `@support-selfmanaged` - Self-managed support team members.
- `@support-dotcom` - GitLab.com support team members.
- `@supportmanagers` - Support director and managers.

## Support Tickets & Customer Information

### Requesting Support for Customers

If your customer contacts you requiring technical support, please immediately direct them to open a ticket through the [Support Portal](https://support.gitlab.com). It is Support's primary function to provide technical support for our customers and as paid users they are entitled access to us. If, for some reason they cannot access the Support Portal, please direct them to email `support@gitlab.com`.

**Please do not open a support ticket on behalf of a customer.** Doing so will cause the ticket to not be tied to the customer's organization and the appropriate SLA that they are entitled to will not be applied to it.

### Requesting Customer Information

According to our [privacy policies](/privacy/), Support will not provide any information regarding customers, groups, projects, etc,  to you that are not available publicly. This includes situations where a customer is requesting information about their own projects, groups, etc. If they are unable to authenticate, we cannot assume they are who they say they are. If they are locked out, please have them submit a support ticket.

### Viewing Suport Tickets

All GitLab staff can request a 'Light Agent' account so that you can see customer tickets in Zendesk and leave notes for the Support team. These accounts are free.

To request a Light Agent Zendesk account, please send an email to `gitlablightagent.5zjj2@zapiermail.com` - you'll receive an automated reply with the result of your request. **You must send your request from your GitLab Google / Gmail account. No other addresses will work.** The Subject and Body fields of the email can be empty. Once set up, you'll need to wait 24 hours for your account to be assigned Zendesk in Okta. Once Zendesk is assigned, you should be able to [log in](https://gitlab.zendesk.com/agent).

You cannot send public replies to customers with a Light Agent account - if you need to do this, please submit a [new Access Request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) for a paid full agent account. If needed, you can [read more information](https://www.zendesk.com/company/collaboration-add-on-additional-features/) on Light Agent accounts from Zendesk.

## Common Internal Requests

### All GitLab Team Members

These requests are typically raised by any GitLab team member outside of Support.

#### I want Gold for my work or personal account

- Refer to [this section](https://about.gitlab.com/handbook/incentives/#gitlab-gold) of the handbook for details on requesting Gold for your personal namespace.

- If you require a group to be upgraded to Gold for work purposes, please [submit an access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single+Person+Access+Request).

#### I want to claim a dormant [namespace](https://docs.gitlab.com/ee/user/group/#namespaces)

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Dormant%20Name%20Request) using the `Dormant Name Request` template.

#### I'm getting reCaptcha on an issue/MR on GitLab.com

- At the time it's happening, please post in [#support_gitlab-com](https://gitlab.slack.com/messages/C4XFU81LG/) and link to the issue/MR in question. A team member will mark you as "ham", which override the reCaptcha requirement.

#### I need access to something

- Support doesn't handle these types of requests so you'll want to open a new [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=New%20Access%20Request) issue.

#### I need Support to contact a user on GitLab's behalf

Typically, this is for the production team to notify actions taken on behalf of a user. If required, please:

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Contact%20Request) using the `Contact Request` template.

>**NOTE**: [Requests for support](https://about.gitlab.com/handbook/support/internal-support/#requesting-support-for-customers) should come directly from the customer/prospect and be [escalated](https://about.gitlab.com/handbook/support/internal-support/#i-want-to-draw-attention-to-an-existing-support-ticket) to draw attention it.

### Sales / Customer Success / Finance

These requests are typically raised by GitLab team members in Sales, Customer Success, or Finance.

#### I want to extend or change the plan of a GitLab.com Trial

To request that an active or expired GitLab.com trial be extended, please:

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=GitLab.com%20Trial%20Extension) using the `GitLab.com Trial Extension` template. These are handled by Support Engineers.

To grant a Bronze or Silver trial to a prospect or customer, please:

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Plan%20Change%20Request) using the `Plan Change Request` template.

>**NOTE**: It's not yet possible for users or GitLab employees to directly start a Bronze or Silver trial on a namespace. If one is needed, **have the user initiate a Gold trial first** and then open an issue per the above link using the `Plan Change Request` template to have the plan manually changed. Implementation of this feature is being discussed in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/119089).

#### I want to extend a self-managed GitLab trial

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Self%20Managed%20Trial%20Extension) using the `Self Managed Trial Extension` template. These are handled by the [Growth](https://about.gitlab.com/handbook/engineering/development/growth/) team.

#### My customer is unable to apply a purchased subscription to their GitLab.com group

The customer has more than likely run into an issue during the purchase process or is unaware of how to apply their subscription to their group. The following documentation outlines how to subscribe to GitLab.com, link your GitLab.com account to the [Customers Portal](https://customers.gitlab.com), and apply that subscription to their group.

- [Obtain a GitLab Subscription](https://docs.gitlab.com/ee/subscriptions/#obtain-a-gitlab-subscription)
- [Manage Your GitLab Account](https://docs.gitlab.com/ee/subscriptions/#manage-your-gitlab-account)

#### I want to change the plan of a GitLab.com user or group for an external party

If you need to promote the namespace of a contributor, GitLab partner or client in the course of doing business, please:

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Plan%20Change%20Request) using the `Plan Change Request` template.

#### I want to change the plan of a GitLab.com user or group for billing reasons

If you need to downgrade the user or group from a paid plan to a Free plan due to non-payment, please:

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Plan%20Change%20Request) using the `Plan Change Request` template.

#### I want to see if a namespace can be claimed for a prospect or customer

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Dormant%20Namespace%20Request) using the `Dormant Namespace Request` template.

>**NOTE**: Using another's trademark in a way that has nothing to do with the product or service for which the trademark was granted is not a violation of trademark policy. User and group names are provided on a first-come, first-served basis and may not be reserved. We can ask nicely to free an active namespace, but we won't take anything away. Your prospects desired group name may not be freeable.

#### I want to draw attention to an existing support ticket

It's important to understand that GitLab Support targets a 95% SLA acheievement KPI. This means that some tickets breaching is expected. Our SLA is for a _First Reply_ but we also internally track next reply.

1. Review the SLA associated with the account and the amount of time left until a breach by logging into [Zendesk](https://gitlab.zendesk.com) with your GSuite credentials. (It's not typically necessary to escalate an issue that is hours away from a breach) If the ticket has had a first reply, then you are looking at an "internal breach".
1. Post a link to the ticket and a reason for why this ticket needs special attention into `#support-managers`. Feel free to tag `@supportmanagers`. When you do this, we ask that you go above and beyond to help. If something breached, it means we are probably stuck. Take a look at the ticket and try and call in a relevant department manager to help. Messages where you ask other groups for help will more likely get resolved sooner.
1. Understand that we'll do our best to prioritize appropriately taking into account all of the tickets in the queues - there may be more pressing items.

Please note that this guideline also applies to issues outside of Zendesk as well, such as those created in `dotcom-internal`.

#### I want to know who is on-call today

- You can run `/chatops run oncall support` in [#support_self-managed](https://gitlab.slack.com/messages/support_self-managed/) to see who is on-call. This will **not** page the on-call person. Alternatively, you can run that command in a direct message with `GitLab Chatops`.

#### I want to schedule a customer call for upgrade assistance

- [Open an issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/new?issuable_template=Upgrade%20Assistance%20Request) using the `Upgrade Assistance` template.

#### I want to request that pipeline minutes be reset for a user or group

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Pipeline%20Quota%20Reset%20Request) using the `Pipeline Quota Reset Request` template.

#### Users in an account I own would like more visibility into their organization's support tickets

In some cases, certain organizations want all members of their organization to be able to see all of the support tickets that have been logged. In other cases, a particular user from the account would like to be able to see and respond to all tickets from their organization. If you'd like to enable this, please:

- [Open an issue](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/issues/new?issuable_template=Shared%20Organization%20Request) using the `Shared Organization Request` template

#### My customer wants to know how many total users they have in their group(s)

A member of the group with `Owner` permissions may visit the **Settings -> Billing** section of the group to see how many seats are in their subscription and how many are currently in use. If they need a detailed report of where specific users have been added within their groups, subgroups, and projects they may make use of the [GitLab Group Leader](https://gitlab.com/gitlab-com/support/toolbox/glgl) tool. Please note that it is currently not possible for users to download a report of their group user count and usage but an [open feature proposal](https://gitlab.com/gitlab-org/gitlab/issues/27074) suggesting that such functionality be implemented exists.

#### I need an EULA sent/resent

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=EULA) using the `EULA` template.

#### I need assistance with a License issue

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=license%20issue) using the `License Issue` template.

### Legal

These requests are typically raised by GitLab team members in Legal.

#### I need logs preserved pursuant to a subpoena or other formal legal request

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=Information%20Request) using the `Information Request` template.

#### I need to submit a DMCA request

- [Open an issue](https://gitlab.com/gitlab-com/gl-security/abuse/issues/new?issuable_template=dmca_meta_issue) using the `DMCA Meta Issue` template
